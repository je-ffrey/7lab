<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use Response;

class UsersController extends Controller
{
    public function index(){

        $users =  User::all();
            
        return Response::json( [ 'Users' => $users->toArray() ], 200);
    }

    public function show($id){

    	$users =  User::find($id);

    	if ( ! $users) {
    	
    		return Response::json(['error' => [ 'message' => 'User does not exist']], 404);
    	}

    	return Response::json([

    		'data' => $users->toArray()

    		], 200);
    }
    

    public function store(){

        if (! Input::get('id') or ! Input::get('name') or ! Input::get('email')  or ! Input::get('password')) {
            
            return Response::json([

                'error' => [
                    'message' => 'Parameters failed'
                ]
            ], 422);

        }

        User::create(Input::all());

        return Response::json([

            'message' => 'User successful created'

            ], 201);
    	
    }

    public function update($id){

        $user = User::find($id);

        if (! Input::get('name') && Input::get('email') && Input::get('password')  ) {
            
            return Response::json([

                'error' => [
                    'message' => 'Parameters failed'
                ]
            ], 422);

        }         
        $user->update(Input::all());

        return Response::json([

            'message' => 'User successful updated'

            ], 201);
    }

    public function destroy($id) {
        
        $user = User::find($id);
        $user->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'User successful deleted'),
            200
        );
    }




}






