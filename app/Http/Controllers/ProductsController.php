<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Product;
use Response;
use Auth;

class ProductsController extends Controller
{
    public function index(){

    	$products =  Product::all();
    	return Response::json( [ 'Products' => $products->toArray() ], 200);
    }

    public function show($id){

    	$products =  Product::find($id);
    	
    	if ( !$products) {
    		
    		return Response::json(['error' => [ 'message' => 'Product does not exist']], 404);
    	}

    	return Response::json([

    		'Product' => $products->toArray()

    		], 200);
    }
    

    public function store() {

        if (! Input::get('id') or ! Input::get('name') or ! Input::get('description')  or ! Input::get('price') or ! Input::get('stock') ) {
        	
        	return Response::json([

    			'error' => [
    				'message' => 'Parameters failed'
    			]
    		], 422);

        }
        // Create a new product and save it.
        Product::create(Input::all());

        return Response::json([

    		'message' => 'Product successful created'

    		], 201);

    }
    public function update($id){
         
        $product = Product::find($id); 

        if (! Input::get('id') && ! Input::get('name') && ! Input::get('description') && ! Input::get('price') && ! Input::get('stock')) {
            
            return Response::json([

                'error' => [
                    'message' => 'Parameters failed'
                ]
            ], 422);

        }       
        $product->update(Input::all());

         return Response::json([

            'message' => 'Product successful updated'

            ], 201);

    }

    public function destroy($id) {
        
        $product = Product::find($id);
        $product->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'Product successful deleted'),
            200
        );
    }
   
}
