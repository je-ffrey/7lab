<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $fillable = ['id','name', 'description', 'price', 'stock'];
     public $timestamps = false;


     public function categories()
{
    return $this->hasMany('App\Categorie');
}
}
